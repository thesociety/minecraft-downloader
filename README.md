This is a work in progress Minecraft Curseforge downloader.

Used API:
https://twitchappapi.docs.apiary.io

## Required files
It will read Curse/Twitch `manifest.json` file

## Optional files
`server.json` - use this file to add list of mods which the downloader should ignore (for example client only mods)
```
{
    "ignoreMods": [
        {
            "name": "Journeymap",
            "projectID": "32274"
        }
    ]
}
```