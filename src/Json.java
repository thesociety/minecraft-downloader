import java.io.FileReader;

import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;


public class Json {
	public JSONObject jsonObject;

    public void loadJson(String url) {
    	 JSONParser parser = new JSONParser();
    	 
         try {
  
             Object obj = parser.parse(new FileReader(url));
  
             this.jsonObject = (JSONObject) obj;  
         } catch (Exception e) {
             e.printStackTrace();
         }
    }    
}