import java.io.File;

public class MainClass {
	public static void main(final String[] args) {
		File mainfestJson = new File("manifest.json");
		if (!mainfestJson.exists()) {
			System.out.println("You are missing manifest.json file!");
			System.exit(0);
		}
		
		Json manifest = new Json();
		manifest.loadJson("manifest.json");
		
		
		System.out.println("Manifest file of " 
				+ (String) manifest.jsonObject.get("name") + " " + (String) manifest.jsonObject.get("version") 
				+ " by " + (String) manifest.jsonObject.get("author") + " loaded");
		
		Downloader downloader = new Downloader();
		downloader.downloadMinecraft(manifest.jsonObject.get("minecraft"));
		
		File serverJson = new File("server.json");
		if (serverJson.exists()) {
			Json server = new Json();
			server.loadJson("server.json");
			downloader.addToIgnoreList(server.jsonObject);
		}
		
		downloader.beginAddonsDownloadProcess(manifest.jsonObject);
		System.out.println("----- COMPLETED ---------");
	}
}
