import org.json.simple.JSONObject;
import org.json.simple.JSONArray;

import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import java.io.*;
import java.net.*;

import java.util.Iterator;
import java.util.ArrayList;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.MediaType;


public class Downloader {
	private static String apiUrl = "https://addons-ecs.forgesvc.net/api/v2/";
	ArrayList<String> ignoreMods = new ArrayList<String>();
	
	public void addToIgnoreList ( JSONObject serverJson ) {
		JSONParser parser = new JSONParser();
		
		if (serverJson.get("ignoreMods") != null) {
			JSONArray ignoreList = (JSONArray) serverJson.get("ignoreMods");
			Iterator<JSONObject> miniIterator = ignoreList.iterator();
			while (miniIterator.hasNext()) {
				JSONObject object = miniIterator.next();
				System.out.println("Adding " + object.get("projectID") + " to ignorelist");
				ignoreMods.add(object.get("projectID").toString());
			}
			
		}
	}
	
	public void beginAddonsDownloadProcess(JSONObject jsonObject ) {
		JSONArray filesList = (JSONArray) jsonObject.get("files");
		JSONParser parser = new JSONParser();
		
		Iterator<JSONObject> iterator = filesList.iterator();
		
        while (iterator.hasNext()) {
        	JSONObject object = iterator.next();
            //System.out.println(object);

            try {
            	if (ignoreMods.contains(object.get("projectID").toString())) {
            		System.out.println("Skipping " + object.get("projectID") + " due to it being in server ignore list");
            		continue;
            	}
            	JSONObject addonDetail = (JSONObject) parser.parse(this.parseFileUrl(Downloader.apiUrl + "addon/" + object.get("projectID")));
            	JSONObject addonCategory = (JSONObject) addonDetail.get("categorySection");
            	JSONObject fileDetails = (JSONObject) parser.parse(this.parseFileUrl(Downloader.apiUrl + "addon/" + object.get("projectID") + "/file/" + object.get("fileID")));
                
                System.out.println("Begin downloading [" + addonCategory.get("name") + "] " + fileDetails.get("displayName"));
                this.downloadFileFromUrl(fileDetails.get("downloadUrl").toString(), fileDetails.get("fileName").toString(), addonCategory.get("path") + "/");
            } catch(Exception e) {
                System.out.println(e);
            }
            
		}
    }
	
	public String parseFileUrl(String fileUrl) {
		//System.out.println(fileUrl);
		Client client = ClientBuilder.newClient();
		Response response = client.target(fileUrl)
		  .request(MediaType.TEXT_PLAIN_TYPE)
		  .get();

		/*System.out.println("status: " + response.getStatus());
		System.out.println("headers: " + response.getHeaders());*/
		return response.readEntity(String.class);
	}
	
	public void downloadFileFromUrl(String downloadUrl,String fileName, String downloadFolder) {
		try {
			File directory = new File(downloadFolder);
		    if (! directory.exists()){
		        directory.mkdirs();
		        // If you require it to make the entire directory path including parents,
		        // use directory.mkdirs(); here instead.
		    }			
			
			URL url = new URL(downloadUrl);
			URLConnection con = url.openConnection();
			DataInputStream dis = new DataInputStream(con.getInputStream()); 
			byte[] fileData = new byte[con.getContentLength()];
			for (int q = 0; q < fileData.length; q++) { 
                fileData[q] = dis.readByte();
            }
            dis.close(); // close the data input stream
            
            FileOutputStream fos = new FileOutputStream(new File(downloadFolder + fileName)); //FILE Save Location goes here; 
            fos.write(fileData);  // write out the file we want to save.
            fos.close(); // close the output stream writer
		} catch(Exception e) {
            System.out.println(e);
        }
	}
	
	public void downloadMinecraft(Object object) {
		JSONParser parser = new JSONParser();
		
		JSONObject minecraft = (JSONObject) object;
		
		//System.out.println(object);
		try {
			JSONObject minecraftDetails = (JSONObject) parser.parse(this.parseFileUrl(Downloader.apiUrl + "minecraft/version/" + minecraft.get("version")));
            
            System.out.println("Begin downloading Minecraft version " + minecraft.get("version"));
            this.downloadFileFromUrl(minecraftDetails.get("jarDownloadUrl").toString(), "minecaft.jar".toString(), "");
            
            this.downloadModloaders((JSONArray) minecraft.get("modLoaders"));
        } catch(Exception e) {
            System.out.println(e);
        }
		
	}
	
	public void downloadModloaders(JSONArray modLoaders) {
		@SuppressWarnings("unchecked")
		Iterator<JSONObject> iterator = modLoaders.iterator();
		JSONParser parser = new JSONParser();
		
		while (iterator.hasNext()) {
        	JSONObject object = iterator.next();
            //System.out.println(object);
            
            try {
            	JSONObject fileDetails = (JSONObject) parser.parse(this.parseFileUrl(Downloader.apiUrl + "minecraft/modloader/" + object.get("id")));
                
                System.out.println("Begin downloading " + object.get("id"));
                this.downloadFileFromUrl(fileDetails.get("downloadUrl").toString(), "forge.jar".toString(), "");
            } catch(Exception e) {
                System.out.println(e);
            }
            
		}
	}
}
